# Phone Book Application

### Technical test task

Create the phone book application.

No UI needed, only REST API. Two REST API methods should be implemented: one for getting a contact by an account property and other for submitting contact (storing changes will be an advantage).

User inputs should be validated to reject incomplete or invalid data.

#### Technical Requirements

1. Use Java 1.8, Spring Framework and Maven.
2. Use other Java libraries as needed.
3. Use HSQLDB for storing data. It is ok NOT to persist data across
   application launches.
4. Try following all the good principles of writing qualitative and
   testable code.
5. Fill in missing requirements as you feel suitable.
6. Include a short README file describing how the application works
   and how to build and run the project.

***

#### Build and Run

./mvnw clean spring-boot:run

#### Swagger

http://localhost:9001/phone-book/swagger-ui/

#### Examples

**Create contact**

curl -X POST "http://localhost:9001/phone-book/contacts" -v -H "Content-Type: application/json" -d '{"id": "123", "firstName": "John", "lastName": "Doe", "company": "Company Inc.", "phone": "+79991110001", "email": "john_doe@company.com", "birthday": "1990-01-01", "address": "408 W 58th St, New York, NY 10019, USA", "note": "Note"}'

curl -X POST "http://localhost:9001/phone-book/contacts" -v -H "Content-Type: application/json" -d '{"id": "123", "firstName": "Jane", "lastName": "Doe", "company": "Company Inc.", "phone": "+79992220001", "email": "jane_doe@company.com", "birthday": "1992-02-03", "address": "408 W 58th St, New York, NY 10019, USA", "note": "Note"}'

**Get contacts by keyword**

curl "http://localhost:9001/phone-book/contacts?keyword=doe" -v
