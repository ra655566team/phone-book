package com.ra655566.phonebook.controller;

import java.util.List;
import javax.validation.Valid;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ra655566.phonebook.model.Contact;
import com.ra655566.phonebook.service.ContactService;

/**
 * Contact management controller.
 */
@RestController
@RequestMapping("contacts")
@Api(value = "Contact management controller")
public class ContactController {

    private final ContactService service;

    public ContactController(ContactService service) {
        this.service = service;
    }

    /**
     * Creates contact.
     *
     * @param contact Contact to be created.
     * @return Created contact.
     */
    @PostMapping
    @ApiOperation(value = "Creates contact")
    public ResponseEntity<Contact> create(
        @RequestBody
        @Valid Contact contact
    ) {
        return ResponseEntity.ok(service.create(contact));
    }

    /**
     * Finds contacts by keyword.
     *
     * @param keyword Keyword by which search is performed over all contact parameters.
     * @return Found contacts.
     */
    @GetMapping
    @ApiOperation(value = "Finds contacts by keyword")
    public ResponseEntity<List<Contact>> get(
        @RequestParam
        @ApiParam(
            name = "keyword",
            value = "Keyword by which search is performed over all contact parameters",
            required = true
        ) String keyword
    ) {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(service.find(keyword));
    }
}
