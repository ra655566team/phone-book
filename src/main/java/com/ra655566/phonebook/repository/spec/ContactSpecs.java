package com.ra655566.phonebook.repository.spec;

import java.time.LocalDate;
import org.springframework.data.jpa.domain.Specification;

import com.ra655566.phonebook.model.ContactEntity;
import com.ra655566.phonebook.model.ContactEntity_;

/**
 * Specifications to use along with JPA Criteria API.
 */
public class ContactSpecs {

    /**
     * Specification to find contact by first name that contains specified value.
     *
     * @param value Value to search by.
     * @return Specification.
     */
    public static Specification<ContactEntity> firstNameContains(String value) {
        return (root, query, builder) -> builder.like(
            builder.lower(root.get(ContactEntity_.firstName)),
            "%" + value.toLowerCase() + "%"
        );
    }

    /**
     * Specification to find contact by last name that contains specified value.
     *
     * @param value Value to search by.
     * @return Specification.
     */
    public static Specification<ContactEntity> lastNameContains(String value) {
        return (root, query, builder) -> builder.like(
            builder.lower(root.get(ContactEntity_.lastName)),
            "%" + value.toLowerCase() + "%"
        );
    }

    /**
     * Specification to find contact by company that contains specified value.
     *
     * @param value Value to search by.
     * @return Specification.
     */
    public static Specification<ContactEntity> companyContains(String value) {
        return (root, query, builder) -> builder.like(
            builder.lower(root.get(ContactEntity_.company)),
            "%" + value.toLowerCase() + "%"
        );
    }

    /**
     * Specification to find contact by phone that contains specified value.
     *
     * @param value Value to search by.
     * @return Specification.
     */
    public static Specification<ContactEntity> phoneContains(String value) {
        return (root, query, builder) -> builder.like(
            builder.lower(root.get(ContactEntity_.phone)),
            "%" + value.toLowerCase() + "%"
        );
    }

    /**
     * Specification to find contact by email that contains specified value.
     *
     * @param value Value to search by.
     * @return Specification.
     */
    public static Specification<ContactEntity> emailContains(String value) {
        return (root, query, builder) -> builder.like(
            builder.lower(root.get(ContactEntity_.email)),
            "%" + value.toLowerCase() + "%"
        );
    }

    /**
     * Specification to find contact by birthday that equals to specified value.
     *
     * @param value Value to search by.
     * @return Specification.
     */
    public static Specification<ContactEntity> birthdayEquals(LocalDate value) {
        return (root, query, builder) -> builder.equal(
            root.get(ContactEntity_.birthday),
            value
        );
    }

    /**
     * Specification to find contact by address that contains specified value.
     *
     * @param value Value to search by.
     * @return Specification.
     */
    public static Specification<ContactEntity> addressContains(String value) {
        return (root, query, builder) -> builder.like(
            builder.lower(root.get(ContactEntity_.address)),
            "%" + value.toLowerCase() + "%"
        );
    }

    /**
     * Specification to find contact by note that contains specified value.
     *
     * @param value Value to search by.
     * @return Specification.
     */
    public static Specification<ContactEntity> noteContains(String value) {
        return (root, query, builder) -> builder.like(
            builder.lower(root.get(ContactEntity_.note)),
            "%" + value.toLowerCase() + "%"
        );
    }
}
