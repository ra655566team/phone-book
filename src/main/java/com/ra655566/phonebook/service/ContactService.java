package com.ra655566.phonebook.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.ra655566.phonebook.model.Contact;
import com.ra655566.phonebook.model.ContactEntity;
import com.ra655566.phonebook.model.mapper.ContactMapper;
import com.ra655566.phonebook.repository.ContactRepository;
import com.ra655566.phonebook.repository.spec.ContactSpecs;
import com.ra655566.phonebook.util.ExceptionUtils;

/**
 * Contact management service.
 */
@Service
public class ContactService {

    private final ContactRepository repository;
    private final ContactMapper mapper;

    public ContactService(
        ContactRepository repository,
        ContactMapper mapper
    ) {
        this.repository = repository;
        this.mapper = mapper;
    }

    /**
     * Creates new contact.
     *
     * @param contact Contact to be created.
     * @return Created contact.
     */
    public Contact create(Contact contact) {
        ContactEntity entity = mapper.toEntity(contact);
        ContactEntity savedEntity = repository.save(entity);
        return mapper.toContact(savedEntity);
    }

    /**
     * Finds contacts by keyword.
     *
     * @param keyword Keyword by which search is performed over all contact parameters.
     * @return Found contacts.
     */
    public List<Contact> find(String keyword) {
        Specification<ContactEntity> spec = Specification
            .where(ContactSpecs.firstNameContains(keyword))
            .or(ContactSpecs.lastNameContains(keyword))
            .or(ContactSpecs.companyContains(keyword))
            .or(ContactSpecs.phoneContains(keyword))
            .or(ContactSpecs.emailContains(keyword))
            .or(ContactSpecs.addressContains(keyword))
            .or(ContactSpecs.noteContains(keyword));

        Optional<LocalDate> date = ExceptionUtils.suppressExceptional(
            () -> LocalDate.parse(keyword, DateTimeFormatter.ISO_LOCAL_DATE)
        );
        if (date.isPresent()) {
            spec = spec.or(ContactSpecs.birthdayEquals(date.get()));
        }

        return repository.findAll(spec)
            .parallelStream()
            .map(mapper::toContact)
            .collect(Collectors.toList());
    }
}
