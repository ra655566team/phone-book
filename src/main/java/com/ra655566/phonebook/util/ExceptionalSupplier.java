package com.ra655566.phonebook.util;

/**
 * Supplier like {@link java.util.function.Supplier} but that throws an exception.
 *
 * @param <R> Type of result supplied by this supplier.
 */
@FunctionalInterface
public interface ExceptionalSupplier<R> {

    R get() throws Exception;
}
