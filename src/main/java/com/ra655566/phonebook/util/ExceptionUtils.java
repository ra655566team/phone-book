package com.ra655566.phonebook.util;

import java.util.Optional;

/**
 * Utilities to work with exceptions.
 */
public class ExceptionUtils {

    /**
     * Calls specified function exceptionally (wrapped in try-catch block).
     *
     * @param function Function to be called exceptionally.
     * @param <R>      Type of result returned by function.
     * @return Optional describing the returned value of function if it performs successfully,
     * otherwise an empty Optional.
     */
    public static <R> Optional<R> suppressExceptional(ExceptionalSupplier<R> function) {
        try {
            return Optional.ofNullable(function.get());
        } catch (Exception ignore) {
            return Optional.empty();
        }
    }
}
