package com.ra655566.phonebook.config;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.ra655566.phonebook.model.HttpErrorResponse;

/**
 * Validation exception handler.
 */
@RestControllerAdvice
public class ValidationExceptionHandler {

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public HttpErrorResponse handleException(MethodArgumentNotValidException e) {
        Map<String, List<String>> validationMessages = e.getBindingResult()
            .getAllErrors()
            .stream()
            .map(error -> Pair.of(
                ((FieldError) error).getField(),
                Optional.ofNullable(error.getDefaultMessage()).orElse("")
            ))
            .collect(Collectors.toMap(
                Pair::getFirst,
                pair -> Collections.singletonList(pair.getSecond()),
                (list1, list2) -> Stream.of(list1, list2)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList())
            ));

        HttpStatus status = HttpStatus.BAD_REQUEST;

        return HttpErrorResponse.builder()
            .timestamp(System.currentTimeMillis())
            .status(status.value())
            .error(status.getReasonPhrase())
            .message("Validation errors: " + validationMessages)
            .build();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public HttpErrorResponse handleException(DataIntegrityViolationException e) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        return HttpErrorResponse.builder()
            .timestamp(System.currentTimeMillis())
            .status(status.value())
            .error(status.getReasonPhrase())
            .message(
                Optional.ofNullable(e.getRootCause())
                    .map(Throwable::getMessage)
                    .orElse(e.getMessage())
            )
            .build();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public HttpErrorResponse handleException(InvalidFormatException e) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        return HttpErrorResponse.builder()
            .timestamp(System.currentTimeMillis())
            .status(status.value())
            .error(status.getReasonPhrase())
            .message(
                String.format(
                    "Invalid input value '%s' of parameter '%s'",
                    e.getValue(),
                    e.getPath().get(0).getFieldName()
                )
            )
            .build();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public HttpErrorResponse handleException(MethodArgumentTypeMismatchException e) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        return HttpErrorResponse.builder()
            .timestamp(System.currentTimeMillis())
            .status(status.value())
            .error(status.getReasonPhrase())
            .message(
                String.format(
                    "Invalid input value '%s' of parameter '%s'",
                    e.getValue(),
                    e.getName()
                )
            )
            .build();
    }
}
