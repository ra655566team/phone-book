package com.ra655566.phonebook.model;

import java.time.LocalDate;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Contact model.
 */
@Data
@ApiModel(description = "Contact model")
public class Contact {

    @ApiModelProperty(
        name = "id",
        value = "Contact id",
        example = "0"
    )
    private Long id;

    @NotBlank(message = "First name is mandatory")
    @Size(min = 1, max = 64)
    @ApiModelProperty(
        name = "firstName",
        value = "First name",
        required = true,
        example = "John"
    )
    private String firstName;

    @NotBlank(message = "Last name is mandatory")
    @Size(min = 1, max = 64)
    @ApiModelProperty(
        name = "lastName",
        value = "Last name",
        required = true,
        example = "Doe"
    )
    private String lastName;

    @Size(max = 128)
    @ApiModelProperty(
        name = "company",
        value = "Company",
        example = "Company Inc."
    )
    private String company;

    @NotBlank(message = "Phone is mandatory")
    @Pattern(
        message = "Phone format is invalid",
        regexp = "^\\+[1-9]{1}[0-9]{3,14}$"
    )
    @ApiModelProperty(
        name = "phone",
        value = "Phone",
        required = true,
        example = "+79991112233"
    )
    private String phone;

    @Pattern(
        message = "Email format is invalid",
        regexp = "^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])$"
    )
    @Size(max = 256)
    @ApiModelProperty(
        name = "email",
        value = "Email",
        example = "john_doe@company.com"
    )
    private String email;

    @ApiModelProperty(
        name = "birthday",
        value = "Birthday",
        example = "1990-12-31"
    )
    private LocalDate birthday;

    @Size(max = 256)
    @ApiModelProperty(
        name = "address",
        value = "Address",
        example = "408 W 58th St, New York, NY 10019, USA"
    )
    private String address;

    @Size(max = 2048)
    @ApiModelProperty(
        name = "note",
        value = "Note",
        example = "Note"
    )
    private String note;
}
