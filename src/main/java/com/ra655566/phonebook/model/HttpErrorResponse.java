package com.ra655566.phonebook.model;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * Http error response model returned by exception handlers.
 */
@Builder
@Getter
@ToString
public class HttpErrorResponse {

    private final Long timestamp;
    private final int status;
    private final String error;
    private final String message;
}
