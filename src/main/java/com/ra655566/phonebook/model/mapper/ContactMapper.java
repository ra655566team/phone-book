package com.ra655566.phonebook.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.NullValuePropertyMappingStrategy;

import com.ra655566.phonebook.model.Contact;
import com.ra655566.phonebook.model.ContactEntity;

/**
 * Contact mapper.
 */
@Mapper(
    componentModel = "spring",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
)
public interface ContactMapper {

    @Mappings({
        @Mapping(target = "id", ignore = true)
    })
    ContactEntity toEntity(Contact contact);

    Contact toContact(ContactEntity entity);

    @Mappings({
        @Mapping(target = "id", ignore = true)
    })
    ContactEntity update(Contact contact, @MappingTarget ContactEntity contactEntity);
}
